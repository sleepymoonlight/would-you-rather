const express = require('express');
const bodyParser = require('body-parser');

const JsonDB = require('node-json-db');
const db = new JsonDB("managedQuestions", true, false);
const cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());
let questions = db.getData('/questions');


app.get('/', function (req, res) {
    res.send('Hello API');
});

app.get('/questions', function (req, res) {
    res.send(questions);
});

app.get('/questions/:id', function (req, res) {
    const question = questions.find(function (question) {
        return question.id === Number(req.params.id)
    });
    res.send(question);
});

app.get('/stats', function (req, res) {
    let stats = db.getData('/stats');
    res.send(stats);
});


app.post('/questions', function (req, res) {
    const question = {
        id: Date.now(),
        text: req.body.text,
        firstAnswer: req.body.firstAnswer,
        secondAnswer: req.body.secondAnswer,
    };
    db.push('/questions[]', question, true);
    res.send(question);
});


app.put('/questions/:id', function (req, res) {
    const question = questions.find(function (question) {
        return question.id === Number(req.params.id)
    });
    question.text = req.body.text;
    question.firstAnswer = req.body.firstAnswer;
    question.secondAnswer = req.body.secondAnswer;
    db.push('/questions', question);
    res.send(question);
});

app.put('/stats/:id', function (req, res) {
    let stats = db.getData('/stats');
    let questionStatsIndex = stats.findIndex(function (stats) {
        return stats.id === Number(req.params.id)
    });
    if (questionStatsIndex >= 0) {
        db.delete(`/stats[${questionStatsIndex}]`);
    }
     const questionStats = {
        id: Number(req.params.id),
        firstAnswerCount: +req.body.firstAnswerCount,
        secondAnswerCount: +req.body.secondAnswerCount,
    };
    db.push('/stats[]', questionStats);
    res.send(stats);
});


app.listen(port, () => console.log(`Listening on port ${port}`));