import { handleActions } from 'redux-actions';
import { fetchAll, addQuestion } from "./questionsActions";

export const initialState = [];

export default handleActions(
    {
        [fetchAll]: (state, action) => {
            return action.payload
        },
        [addQuestion]: (state) => {
            return state
        }
    },
    initialState
)
