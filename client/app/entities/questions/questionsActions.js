import {createAction} from 'redux-actions';
import axios from 'axios';

export const fetchAll = createAction('FETCH_ALL');
export const addQuestion = createAction('ADD_QUESTION');

export function fetchAllQuestions() {
    return (dispatch) => {
        axios.get('http://localhost:5000/questions')
            .then((response) => dispatch(
                fetchAll(response.data)
            ));
    }
}

export function addNewQuestion(question) {
    return (dispatch) => {
        return axios.post('http://localhost:5000/questions', question)
            .then((response) => dispatch(
                addQuestion(response)
            )).catch((error) => dispatch(
                console.log(error)
            ));

    }
}
