import {createAction} from 'redux-actions';
import axios from 'axios';

export const getStats = createAction('GET_STATS');
export const chooseAnswer = createAction('CHOOSE_ANSWER');

export function getAllStats() {
    return (dispatch) => {
        axios.get('http://localhost:5000/stats')
            .then((response) => dispatch(
                getStats(response.data)
            ));
    }
}

export function chooseCurrentAnswer(stats) {
    return (dispatch) => {
        const statsUrl = 'http://localhost:5000/stats/' + stats.id;
        return axios.put(statsUrl, stats)
            .then((response) => dispatch(
                chooseAnswer(response.data)
            ));

    }
}
