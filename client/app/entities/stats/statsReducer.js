import {handleActions} from 'redux-actions';
import {getStats, chooseAnswer} from "./statsActions";

const initialState = [];

export default handleActions(
    {
        [getStats]: (state, action) => {
            return action.payload
        },
        [chooseAnswer]: (state, action) => {
            return action.payload
        }
    },
    initialState
)
