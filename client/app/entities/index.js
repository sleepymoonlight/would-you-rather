import {combineReducers} from 'redux';
import questions from './questions/questionsReducer';
import stats from './stats/statsReducer';

export default combineReducers({
    questions,
    stats
});