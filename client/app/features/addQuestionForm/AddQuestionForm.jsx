import React, {Fragment} from 'react';
import style from './addQuestionForm.module.less';
import Button from "../../components/button/Button";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import * as QuestionsActions from '../../entities/questions/questionsActions';
import Input from "../../components/input/Input";

class AddQuestionForm extends React.Component {
    state = {
        text: '',
        firstAnswer: '',
        secondAnswer: '',
        redirect: false
    };

    onSubmit = (event) => {
        event.preventDefault(event);
        if (this.state.text !== '' &&
            this.state.firstAnswer !== '' &&
            this.state.secondAnswer !== '') {
            this.props.actions.addNewQuestion({
                text: this.state.text,
                firstAnswer: this.state.firstAnswer,
                secondAnswer: this.state.secondAnswer
            });
            this.setState({redirect: true})
        }
    };

    render() {
        const {redirect} = this.state;
        return (
            <Fragment>
                <form
                    className={style.form}
                    onSubmit={this.onSubmit}
                >
                    <Input
                        types='textarea'
                        description='Your question:'
                        value={this.state.text}
                        onChange={e => this.setState({text: e.target.value})}
                        placeholder='Enter your question here..'
                    />
                    <Input
                        description='First answer:'
                        value={this.state.firstAnswer}
                        onChange={e => this.setState({firstAnswer: e.target.value})}
                        placeholder='First answer'
                    />
                    <Input
                        description='Second answer:'
                        value={this.state.secondAnswer}
                        onChange={e => this.setState({secondAnswer: e.target.value})}
                        placeholder='Second answer'
                    />
                    <div className={style.buttons}>
                        <Button type='cancel' href="/">Cancel</Button>
                        <Button type='submit'>Submit</Button>
                    </div>
                </form>
                {redirect && (
                    <Redirect to={'/'}/>
                )}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        questions: state.questions,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...QuestionsActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(
    AddQuestionForm);