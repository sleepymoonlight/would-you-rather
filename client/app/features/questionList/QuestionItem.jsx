import React from 'react';
import style from './questionItem.module.less';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowDown} from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import ProgressBar from '../../components/progressBar/ProgressBar';
import Answer from '../../components/answer/Answer';
const INITIAL_STATE = {
    choosedAnswer: '',
    choosed: false,
};

class QuestionItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }

    showAnswers = (event) => {
        event.preventDefault(event);
        this.props.closeAll();
        this.props.openItem(this.props.question.id);
    };


    chooseAnswer = (answerCount, answer) => {
        const {stats, question, actions} = this.props;
        actions.chooseCurrentAnswer({
            id: question.id,
            ...stats,
            ...answerCount
        });
        this.setState({choosed: true, choosedAnswer: answer});
    };


    chooseFirstAnswer = () => {
        const {stats, question} = this.props;
        this.chooseAnswer({firstAnswerCount: ++stats.firstAnswerCount}, question.firstAnswer)
    };

    chooseSecondAnswer = () => {
        const {stats, question} = this.props;
        this.chooseAnswer({secondAnswerCount: ++stats.secondAnswerCount}, question.secondAnswer)
    };

    choosedAnswer = (answer) => {
        return this.state.choosedAnswer === answer;
    };

    render() {
        const question = this.props.question;
        const isOpen = this.props.isOpen;
        const stats = this.props.stats;
        const sumStats = stats.firstAnswerCount + stats.secondAnswerCount;
        const iconClassName = (isOpen) ? style.iconOpen : style.icon;

        return (
            <div className={style.questionBox}>
                <div className={style.question}>
                    <div className={style.text} onClick={this.showAnswers.bind(this)}>
                        <FontAwesomeIcon icon={faArrowDown} className={iconClassName}/>
                        <p>{question.text}</p>
                    </div>
                    {(isOpen) ?
                        <div className={style.answers}>
                            <Answer
                                checked={this.choosedAnswer(question.firstAnswer)}
                                onClick={this.chooseFirstAnswer}
                                value={1}
                                choosed={this.state.choosed}
                                answer={question.firstAnswer}
                            />
                            {(this.state.choosed) ?
                                <ProgressBar value={stats.firstAnswerCount}
                                             sumValue={sumStats}
                                             choosed={this.choosedAnswer(question.firstAnswer)}/>
                                : <div/>}
                          <Answer
                              checked={this.choosedAnswer(question.secondAnswer)}
                              onClick={this.chooseSecondAnswer}
                              value={2}
                              choosed={this.state.choosed}
                              answer={question.secondAnswer}
                          />
                            {(this.state.choosed) ?
                                <ProgressBar value={stats.secondAnswerCount}
                                             sumValue={sumStats}
                                             choosed={this.choosedAnswer(question.secondAnswer)}/>
                                : <div/>}
                        </div> : <div/>
                    }
                </div>
            </div>
        );
    }
}

QuestionItem.propTypes = {
    question: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    openItem: PropTypes.func.isRequired,
    closeAll: PropTypes.func.isRequired,
};

QuestionItem.defaultProps = {
    stats: {
        firstAnswerCount: 0,
        secondAnswerCount: 0,
    },
};

export default QuestionItem;