import React, {Component, Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as QuestionsActions from '../../entities/questions/questionsActions';
import * as statsActions from '../../entities/stats/statsActions';
import QuestionItem from './QuestionItem';

const INITIAL_STATE = {
    openedItem: -1,
};

export class QuestionListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }

    componentDidMount() {
        this.props.actions.fetchAllQuestions();
        this.props.actions.getAllStats();
    }

    closeAll = () => {
        this.setState({
            openedItem: -1,
        });
    };

    openItem = (id) => {
        this.setState({
            openedItem: id,
        });
    };

    getQuestionStats = (id) => {
        const stats = this.props.stats;
        return stats.find(function (stat) {
            return stat.id === id;
        });
    };

    render() {
        const questionsList = this.props.questions;
        return (
            <Fragment>
                {questionsList.map((item) => {
                    return (
                        <QuestionItem
                            question={item}
                            key={item.id}
                            stats={this.getQuestionStats(item.id)}
                            closeAll={this.closeAll}
                            openItem={this.openItem}
                            isOpen={this.state.openedItem === item.id}
                            actions = {this.props.actions}
                        />
                    );
                })}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        questions: state.questions,
        stats: state.stats
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...QuestionsActions, ...statsActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(
    QuestionListContainer);
