import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../entities/index';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';

export const history = createHistory();

const initialState = {
};
const enhancers = [];
const middleware = [
  thunk
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const composedEnhancers = composeEnhancers(
    applyMiddleware(...middleware),
    ...enhancers
);
const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
