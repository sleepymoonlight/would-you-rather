import React, {Component} from 'react';
import './App.module.less';
import {Switch} from 'react-router-dom';
import {Route} from 'react-router';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons';
import Home from "./scenes/home/Home";
import AddQuestion from "./scenes/addQuestion/AddQuestion";

library.add(faStroopwafel);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/home" component={Home}/>
            <Route path="/add-question" component={AddQuestion}/>
        </Switch>
      </div>
    );
  }
}

export default App;
