import React from 'react';
import PropTypes from 'prop-types';
import style from './input.module.less';

class Input extends React.Component {
    render() {
        return (
            <label className={style.label}>
                <p className={style.text}>{this.props.description}</p>
                {(this.props.types) ?
                <textarea
                    value={this.props.value}
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}/> :
                <input
                    value={this.props.value}
                    type='text'
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}/>}
            </label>
        );
    }
}

Input.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
};

export default Input;
