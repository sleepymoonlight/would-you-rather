import React from 'react';
import PropTypes from 'prop-types';
import styles from './button.module.less';
import Link from 'react-router-dom/es/Link';

class Button extends React.Component {
    render() {
        return (
            this.props.href ?
                <Link to={this.props.href}
                      className={styles.button}
                >
                    {this.props.children}
                </Link> :
                <button className={styles.button}>{this.props.children}</button>
        );
    }
}

Button.propTypes = {
    href: PropTypes.string,
};

export default Button;
