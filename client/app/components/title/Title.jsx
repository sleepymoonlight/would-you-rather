import React from 'react';
import styles from './title.module.less'

class Title extends React.Component {
    render() {
        return (
            <div className={styles.title}>
                {this.props.children}
            </div>
        )
    }
}

export default Title;