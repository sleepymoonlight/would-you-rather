import React from 'react';
import { shallow } from 'enzyme';
import Title from '../Title';

describe('Title', () => {
    it('should render correctly', () => {
        const component = shallow(<Title />);

        expect(component).toMatchSnapshot();
    });
});