import React from 'react';
import style from './progressBar.module.less'
import PropTypes from 'prop-types';

class ProgressBar extends React.Component {
    calculateWidth = () => {
        const sumValue = this.props.sumValue;
        const value = this.props.value;
        return Math.round(value / (sumValue) * 100);
    };

    render() {
        const fillerStyle = `${style.filler} ${(this.props.choosed) ? style.fillerActive : ''}`;

        return (
            <div className={style.progressBar}>
                <div
                    className={fillerStyle}
                    style={{
                        width: `${this.calculateWidth()}%`
                    }}>
                    <p className={style.percentCount}>
                        {this.calculateWidth()}% of people choose it
                    </p>
                </div>
            </div>
        )
    }
}

ProgressBar.propTypes = {
  sumValue: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  choosed: PropTypes.bool.isRequired
};

export default ProgressBar;
