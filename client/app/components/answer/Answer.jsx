import React from 'react';
import PropTypes from 'prop-types';
import style from './answer.module.less';

class Answer extends React.Component {
    render() {
        const activeItemClass = `${style.answerItem} ${(this.props.checked) ? style.answerItemActive : ''}`;

        return (
            <label
                className={activeItemClass}>
                <input type="radio"
                       value={this.props.value}
                       name='answer'
                       onClick={this.props.onClick.bind(this)}
                       disabled={this.props.choosed}
                       checked={this.props.checked}
                />
                {this.props.answer}
            </label>
        );
    }
}

Answer.propTypes = {
    checked: PropTypes.bool.required,
    onClick: PropTypes.func.required,
    value: PropTypes.number.required,
    answer: PropTypes.object.required,
    choosed: PropTypes.bool.required
};

export default Answer;
