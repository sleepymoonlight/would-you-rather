import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import App from './App';
import store, {history} from './store';
import { Router } from 'react-router-dom';
import {Provider} from 'react-redux'

ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
