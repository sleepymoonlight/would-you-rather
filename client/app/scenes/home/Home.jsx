import React from 'react';
import QuestionList from '../../features/questionList/QuestionListContainer.jsx';
import style from './home.module.less'
import Title from '../../components/title/Title';
import Button from '../../components/button/Button';


const Home = () => (
    <div className={style.box}>
        <div className={style.container}>
            <Title>Would you rather...</Title>
            <QuestionList/>
            <div className={style.newQuestion}>
                <p className={style.text}>That`s not enough for you? </p>
                <Button href="/add-question">Add question</Button>
            </div>
        </div>
    </div>
);

export default Home;
