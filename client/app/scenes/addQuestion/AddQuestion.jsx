import React from 'react';
import style from './addQuestion.module.less';
import Title from "../../components/title/Title";
import AddQuestionForm from "../../features/addQuestionForm/AddQuestionForm";

const AddQuestion = () => {
    return (
        <div className={style.box}>
            <div className={style.container}>
                <Title>Add your question</Title>
                <AddQuestionForm/>
            </div>
        </div>
    )
};

export default AddQuestion;
